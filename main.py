#!/usr/bin/env python3
import sys
from time import sleep
import RPi.GPIO as GPIO
import basic_moves

GPIO.setmode(GPIO.BOARD)

motor_pin_list = [16, 12, 22, 15, 18, 13]
GPIO.setup(motor_pin_list, GPIO.OUT)

def main():
	print("Sequence initaited!")
	sleep(5.2)
	basic_moves.move_forward(5.81)
	sleep(0.7)
	basic_moves.turn_left(0.39)
	sleep(0.6)
	basic_moves.move_forward(1.1)
	sleep(0.7)
	basic_moves.turn_left(0.5)
	sleep(0.8)
	basic_moves.move_forward(2.1)

	sleep(0.7)
	basic_moves.turn_left(0.3)
	sleep(0.6)
	basic_moves.move_forward(1.5)
	sleep(0.7)
	basic_moves.turn_right(0.45)
	sleep(0.6)
	basic_moves.move_forward(1.3)
	sleep(0.6)
	basic_moves.turn_right(0.5)
	sleep(0.6)
	basic_moves.move_forward(1.5)

	sleep(0.8)
	basic_moves.turn_right(0.54)
	sleep(0.6)
	basic_moves.move_forward(2.34)
	sleep(1.45)
	basic_moves.turn_left(0.4)
	sleep(0.6)
	basic_moves.move_forward(3.33)
	GPIO.cleanup()
	
def not_main():
	print("What started me?")
	
if __name__=='__main__':
	main()
else:
	not_main()
