#!/usr/bin/env python3
import RPi.GPIO as GPIO
from time import sleep

def move_forward(time):
	make_the_move(time, [12, 15, 16, 22])
def reverse(time):
	make_the_move(time, [12, 13, 16, 18])
def turn_left(time):
	make_the_move(time, [12, 13, 16, 22])
def turn_right(time):
	make_the_move(time, [12, 15, 16, 18])
def make_the_move(time, pins):
	for pin in pins:
		if pin:
			GPIO.output(pin,GPIO.HIGH)
	sleep(time)
	for pin in pins:
		if pin:
			GPIO.output(pin,GPIO.LOW)
